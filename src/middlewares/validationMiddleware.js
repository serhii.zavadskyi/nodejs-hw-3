const Joi = require('joi');

const options = {
  errors: {
    wrap: {
      label: `'`,
    },
  },
};

const validateRegister = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string().email().lowercase().required(),
    password: Joi.string().required(),
    role: Joi.string().valid(...['DRIVER', 'SHIPPER']).required().insensitive(),
  });

  await schema.validateAsync(req.body, options);
  next();
};

const validateLogin = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string().required(),
    password: Joi.string().required(),
  });

  await schema.validateAsync(req.body, options);
  next();
};

const validateForgotPassword = async (req, res, next) => {
  const schema = Joi.object({
    email: Joi.string().email().lowercase().required(),
  });

  await schema.validateAsync(req.body, options);
  next();
};

module.exports = {
  validateLogin,
  validateRegister,
  validateForgotPassword,
};
