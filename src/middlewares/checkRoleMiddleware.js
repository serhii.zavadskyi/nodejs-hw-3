const checkDriver = async (req, res, next) => {
  if (req.user.role === 'DRIVER') {
    next();
  } else {
    return res.status(400).json({message: `You haven't permissions!`});
  }
};

const checkShipper = async (req, res, next) => {
  if (req.user.role === 'SHIPPER') {
    next();
  } else {
    return res.status(400).json({message: `You haven't permissions!`});
  }
};

module.exports = {
  checkDriver,
  checkShipper,
};
