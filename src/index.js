require('dotenv').config();
const express = require('express');

const mongoose = require('mongoose');
const morgan = require('morgan');

const authRouter = require('./routers/authRouter');
const userRouter = require('./routers/userRouter');
const trucksRouter = require('./routers/truckRouter');
const loadsRouter = require('./routers/loadRouter');


const app = express();

const PORT = process.env.PORT || 8080;


app.use(express.json());
app.use(morgan('tiny'));

app.use(authRouter);
app.use(userRouter);
app.use(trucksRouter);
app.use(loadsRouter);


async function start() {
  try {
    await mongoose.connect(process.env.DB_HOST, {
      useNewUrlParser: true,
      useUnifiedTopology: true,
      useFindAndModify: false,
      useCreateIndex: true,
    });

    app.listen(PORT, () => {
      console.log(`App listening at http://localhost:${PORT}`);
    });
  } catch (e) {
    console.log(e);
  }
}

start();
