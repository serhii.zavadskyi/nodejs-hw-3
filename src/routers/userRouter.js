const express = require('express');
const router = new express.Router();
const {authMiddleware} = require('../middlewares/authMiddleware');


const {
  getUser,
  deleteUser,
  changePassword,
} = require('../controllers/userController');

router.get('/api/users/me',
    authMiddleware, getUser);
router.delete('/api/users/me',
    authMiddleware, deleteUser);
router.patch('/api/users/me/password',
    authMiddleware, changePassword);

module.exports = router;
