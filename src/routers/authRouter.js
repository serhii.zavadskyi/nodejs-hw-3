const express = require('express');
const router = new express.Router();

const {
  login,
  register,
  forgotPassword,
} = require('../controllers/authController');
const {
  validateLogin,
  validateRegister,
  validateForgotPassword,
} = require('../middlewares/validationMiddleware');

router.post('/api/auth/login', validateLogin, login);
router.post('/api/auth/register',
    validateRegister,
    register);
router.post('/api/auth/forgot_password',
    validateForgotPassword,
    forgotPassword);


module.exports = router;
