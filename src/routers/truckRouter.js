const express = require('express');
const router = new express.Router();
const {authMiddleware} = require('../middlewares/authMiddleware');
const {checkDriver} = require('../middlewares/checkRoleMiddleware');

const {
  getTrucks,
  addTruck,
  getTruckById,
  updateTruckById,
  deleteTruckById,
  assignTruckById,
} = require('../controllers/truckController');

router.get('/api/trucks', authMiddleware, checkDriver, getTrucks);
router.post('/api/trucks', authMiddleware, checkDriver, addTruck);
router.get('/api/trucks/:id',
    authMiddleware, checkDriver, getTruckById);
router.put('/api/trucks/:id',
    authMiddleware, checkDriver, updateTruckById);
router.delete('/api/trucks/:id',
    authMiddleware, checkDriver, deleteTruckById);
router.post('/api/trucks/:id/assign',
    authMiddleware, checkDriver, assignTruckById);

module.exports = router;
