const express = require('express');
const router = new express.Router();
const {authMiddleware} =require('../middlewares/authMiddleware');

const {
  getLoads,
  addLoad,
  getActiveLoads,
  loadProgress,
  getLoadById,
  updateLoadById,
  deleteLoadById,
  postALoadById,
  getShippingInfo,
} = require('../controllers/loadController');

router.get('/api/loads', authMiddleware, getLoads);
router.post('/api/loads', authMiddleware, addLoad);
router.get('/api/loads/active', authMiddleware, getActiveLoads);
router.patch('/api/loads/active/state',
    authMiddleware,
    loadProgress);
router.get('/api/loads/:id', authMiddleware, getLoadById);
router.put('/api/loads/:id', authMiddleware, updateLoadById);
router.delete('/api/loads/:id', authMiddleware, deleteLoadById);
router.post('/api/loads/:id/post', authMiddleware, postALoadById);
router.get(
    '/api/loads/:id/shipping_info',
    authMiddleware,
    getShippingInfo);

module.exports = router;
