const {Truck} = require('../models/truckModel');

const getTrucks = async (req, res) => {
  const {offset = 0, limit = 5} = req.query;
  try {
    const trucks = await Truck.find(
        {created_by: req.user._id},
        ['_id', 'created_by', 'assigned_to', 'type', 'status', 'created_date'],
        {
          limit: +limit,
          skip: +offset,
        });

    res.status(200).json({trucks});
  } catch (e) {
    console.log(e);
  }
};

const addTruck = async (req, res) => {
  const type = req.body.type;
  const truck = new Truck({
    created_by: req.user._id,
    status: 'IS',
    type,
    assigned_to: null,
  });

  try {
    await truck.save();
    res.status(200).json({message: 'Truck created successfully'});
  } catch (error) {
    console.log(error);
  }
};

const getTruckById = async (req, res) => {
  const id = req.params.id;
  const truck = await Truck.findById(id, {_v: 0}).exec();

  res.status(200).json({truck});
};

const updateTruckById = async (req, res) => {
  try {
    const id = req.params.id;
    const type = req.body.type;
    const currentTruck = await Truck.findById(id, {_v: 0}).exec();

    currentTruck.type = type;
    await currentTruck.save();
    res.status(200).json({message: 'Your truck was updated successfully!'});
  } catch (error) {
    console.log(error);
  }
};


const deleteTruckById = async (req, res) => {
  try {
    const id = req.params.id;

    await Truck.findByIdAndRemove(id);
    res.status(200).json({message: 'Truck deleted successfully!'});
  } catch (error) {
    console.log(error);
  }
};

const assignTruckById = async (req, res) => {
  try {
    const trucks = await Truck.find({created_by: req.user._id}, 'assigned_to');
    const truck = await Truck.findById(req.params.id);

    trucks.forEach((item) => {
      if (item.assigned_to) {
        return res.json({message: 'You have already assigned truck'});
      }
    });
    truck.assigned_to = req.user._id;
    await truck.save();
    return res.json({message: 'Truck assigned successfully'});
  } catch (error) {
    console.log(error);
  }
};

module.exports = {
  getTrucks,
  addTruck,
  getTruckById,
  updateTruckById,
  deleteTruckById,
  assignTruckById,
};
