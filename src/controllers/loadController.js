const {Load} = require('../models/loadModel');
const {Truck} = require('../models/truckModel');
const {TRUCK_TYPES} = require('../config');

const getLoads = async (req, res) => {
  try {
    const {status, offset = 0, limit = 10} = req.query;
    let loads;

    if (status) {
      loads = await Load.find(
          {created_by: req.user._id, status},
          null,
          {limit: +limit, skip: +offset},
      );
    } else {
      loads = await Load.find(
          {created_by: req.user._id},
          null,
          {limit: +limit, skip: +offset},
      );
    }

    res.status(200).json({loads});
  } catch (error) {
    console.log(error);
  }
};

const addLoad = async (req, res) => {
  try {
    const {
      name,
      payload,
      pickup_address: pickupAddress,
      delivery_address: deliveryAddress,
      dimensions,
    } = req.body;
    const load = new Load({
      created_by: req.user._id,
      status: 'NEW',
      name,
      payload,
      pickup_address: pickupAddress,
      delivery_address: deliveryAddress,
      dimensions,
    });

    await load.save();
    res.status(200).json({message: 'Load created successfully'});
  } catch (error) {
    console.log(error);
  }
};

const getLoadById = async (req, res) => {
  try {
    const id = req.params.id;
    const load = await Load.findById(id, {_v: 0}).exec();

    res.status(200).json({load});
  } catch (error) {
    console.log(error);
  }
};

const updateLoadById = async (req, res) => {
  try {
    const id = req.params.id;
    const load = req.body;
    const currentLoad = await Load.findById(id, {_v: 0}).exec();
    const loadKeys = Object.keys(load);

    for (const key of loadKeys) {
      currentLoad[key] = load[key];
    }

    await currentLoad.save();
    res.status(200).json({message: 'Load changed successfully!'});
  } catch (error) {
    console.log(error);
  }
};

const deleteLoadById = async (req, res) => {
  try {
    const id = req.params.id;

    await Load.findByIdAndRemove(id);
    res.status(200).json({message: 'Load removed successfully!'});
  } catch (error) {
    console.log(error);
  }
};

const postALoadById = async (req, res) => {
  try {
    const id = req.params.id;
    const load = await Load.findById(id);
    const newLogs = load.logs;

    if (load.status !== 'NEW') {
      return res.status(400)
          .json({message: `You can post only new load`});
    };

    load.status = 'POSTED';
    await load.save();

    const truckTypes = TRUCK_TYPES.filter(
        (item) => item.payload >= load.payload &&
        item.dimensions.length >= load.dimensions.length &&
        item.dimensions.width >= load.dimensions.width &&
        item.dimensions.height >= load.dimensions.height,
    ).map((item) => item.type);

    const truck = await Truck.findOne({
      status: 'IS', type: {
        $in: truckTypes,
      },
    }).exec();

    if (!truck) {
      newLogs.push({
        message: 'Load posted successfully.', driver_found: false,
        time: new Date(),
      });
      load.logs = newLogs;
      load.status = 'NEW';
      await load.save();
      return res.status(200).json({
        message: `Load posted successfully`, driver_found: true});
    }

    truck.status = 'OL';

    const driverId = truck.assigned_to;

    newLogs.push({
      message: 'Load posted successfully', driver_found: true,
      time: new Date(),
    });

    load.status = 'ASSIGNED';
    load.assigned_to = driverId;
    load.state = 'En route to Pick Up';
    load.logs = newLogs;
    await load.save();
    await truck.save();
    res.status(200)
        .json({message: 'Load posted successfully', driver_found: true});
  } catch (error) {
    console.log(error);
  }
};

const getShippingInfo = async (req, res) => {
  try {
    const id = req.params.id;

    const load = await Load.findById(id, {_v: 0}).exec();
    const truck = await Truck.findOne({assigned_to: load.assigned_to});

    res.status(200).json({load, truck});
  } catch (error) {
    console.log(error);
  }
};

const getActiveLoads = async (req, res) => {
  try {
    const load = await Load.find(
        {assigned_to: req.user._id, status: 'ASSIGNED'},
        {__v: 0},
    );

    if (!load) {
      return res.status(400).json({message: 'There is no active load'});
    }

    res.status(200).json({load: load[0]});
  } catch (error) {
    console.log(error);
  }
};

const loadProgress = async (req, res) => {
  try {
    const load = await Load
        .findOne({assigned_to: req.user._id});

    if (!load) {
      return res.status(400)
          .json({message: 'There is no active load for you'});
    }

    const newLogs = load.logs;
    let message;
    let state;

    switch (load.state) {
      case '':
        state = 'En route to Pick Up';
        message = 'Load En route to Pick Up';
        break;
      case 'En route to Pick Up':
        state = 'Arrived to Pick Up';
        message = 'Load Arrived to Pick Up';
        break;
      case 'Arrived to Pick Up':
        state = 'En route to delivery';
        message = 'Load En route to delivery';
        break;
      case 'En route to delivery':
        state = 'Arrived to delivery';
        message = 'Load arrived to delivery';
        newLogs.push({
          message: message,
          time: new Date(),
        });
        await Load.findByIdAndUpdate(load._id, {
          $set: {
            state: state,
            status: 'SHIPPED',
            logs: newLogs,
          },
        });

        await Truck.findOneAndUpdate(
            {assigned_to: req.user._id},
            {$set: {status: 'IS', assigned_to: null}},
        );

        return res.status(200)
            .json({message: `Load was successfully shipped`});
      default:
        return;
    }

    if (state) {
      load.logs.push({
        message: `Load state changed to ${state}`,
        time: new Date(),
      });
      await Load.findByIdAndUpdate(load._id, {state: state, logs: load.logs});

      res.status(200)
          .json({message: `Load state changed to ${state}`});
    }
  } catch (error) {
    console.log(error);
  }
};

module.exports = {
  getLoads,
  addLoad,
  getActiveLoads,
  loadProgress,
  getLoadById,
  updateLoadById,
  deleteLoadById,
  postALoadById,
  getShippingInfo,
};
