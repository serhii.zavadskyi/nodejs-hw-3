const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');
const {JWT_SECRET} = require('../config');
const {User} = require('../models/userModel');

const register = async (req, res) => {
  try {
    const {email, password} = req.body;
    const role = req.body.role.toUpperCase();

    const user = new User({
      email,
      password: await bcrypt.hash(password, 10),
      role,
    });

    await user.save();
    res.json({message: 'User created successfully!'});
  } catch (error) {
    console.log(error);
  }
};

const login = async (req, res) => {
  try {
    const {email, password} = req.body;
    const user = await User.findOne({email});

    if (!user) {
      return res.status(400).json({
        message: `No user with email '${email}' found!`},
      );
    }

    if ( !(await bcrypt.compare(password, user.password)) ) {
      return res.status(400).json({message: `Wrong password!`});
    }

    const token = jwt.sign(JSON.stringify(user), JWT_SECRET);
    res.json({message: 'Success', jwt_token: token});
  } catch (error) {
    console.log(error);
  }
};

const forgotPassword = async (req, res) => {
  try {
    const email = req.body.email;
    const user = await User.findOne({email});

    if (!user) {
      return res.status(400).json({
        message: `No user with email '${email}' found!`},
      );
    }

    res.status(200).json({message: 'New password sent to your email address'});
  } catch (error) {
    console.log(error);
  }
};

module.exports = {
  register,
  login,
  forgotPassword,
};
