const bcrypt = require('bcrypt');
const {User} = require('../models/userModel');

const getUser = async (req, res) => {
  try {
    const user = await User.findById(req.user._id, 'email created_date');

    if (!user) {
      return res.status(400).json({message: `No user was found!`});
    }

    res.status(200).json({user: {
      _id: user._id, email: user.email, created_date: user.created_date,
    }});
  } catch (error) {
    console.log(error);
  }
};

const deleteUser = async (req, res) => {
  try {
    await User.findByIdAndRemove(req.user._id);

    res.status(200).json({message: 'Profile deleted successfully'});
  } catch (error) {
    console.log(error);
  }
};

const changePassword = async (req, res) => {
  try {
    const {oldPassword, newPassword} = req.body;
    const user = await User.findById(req.user._id, 'password').exec();

    if (!(await bcrypt.compare(oldPassword, user.password))) {
      return res.status(400).json({message: 'Wrong old password!'});
    }

    user.password = await bcrypt.hash(newPassword, 10);
    await user.save();

    res.status(200).json({message: 'Password successfuly changed!'});
  } catch (error) {
    console.log(error);
  }
};


module.exports = {
  getUser,
  deleteUser,
  changePassword,
};
