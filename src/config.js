module.exports = {
  JWT_SECRET: process.env.JWT_SECRET || 'my_secret',
  TRUCK_TYPES: [
    {
      type: 'SPRINTER',
      payload: 1700,
      dimensions: {width: 300, length: 250, height: 170},
    },
    {
      type: 'SMALL_STRAIGHT',
      payload: 2500,
      dimensions: {width: 500, length: 250, height: 170},
    },
    {
      type: 'LARGE_STRAIGHT',
      payload: 4000,
      dimensions: {width: 700, length: 350, height: 200},
    },
  ],
};
